# Absolute Agent Web Service

Solution

## Getting started

Assumption - We already have a Kubernetes Cluster running.

Step 1 - Creating a Private S3 bucket with versioning enabled, Bucket with ELB access log delivery policy attached, Bucket with ALB/NLB access log delivery policy attached

Step 2 - Creating a simple Cloudfront service, with dns alias, so that the bucket can be accessed using the DNS name.

Step 3 - Attach the S3 bucket/s to the Kubernetes Cluster as we require it, as in a Persisent Volume or a bind mount or as a storage class based on the need, as for this example it is best if we use it as Persistent volume so that the developer can assess it as and when needed with just creating a Persistent Volume Claim. This solves two problem, 
		1. The access, since only the developer and team members that has access to the cluster can create the PVC, 
		2. the security aspect since the Kubernetes cluster handles the authorisation and authentication, we have one less to configure and worry about.


So to conclude, we created a S3 bucket that we can expose to the end user using Cloud front and we configured the cloud front in such a way that we can access it using DNS name, at last we may attach the S3 bucket to the Kubernetes cluster as required. 
 
